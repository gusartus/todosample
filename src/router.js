import VueRouter from 'vue-router'
import Login from './components/Login.vue'
import page404 from './components/404.vue'
import Todo from './components/Todo.vue'

const mainRouter = {
  routes: [
    {
      path: '/',
      name: 'default',
      redirect: 'todo'
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/todo',
      name: 'todo',
      beforeEnter: (to, from, next) => {
        localStorage.isAuthorized ? next() : next('login')
      },
      component: Todo
    },
    {
      path: '/404',
      name: '404',
      component: page404
    },
    {
      path: '*',
      name: 'unknown',
      redirect: '404'
    }
  ]
}

const Router = new VueRouter(mainRouter)

export default Router
