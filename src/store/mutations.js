export default {
  ADD_TODO (state, todo) {
    state.todoList.push(todo)
  },
  CHANGE_TODO (state, payload) {
    state.todoList[payload.index].text = payload.text
  },
  DELETE_TODO (state, index) {
    state.todoList.splice(index, 1)
  },
  CLEAR_TODOS (state) {
    state.todoList = []
  }
}
